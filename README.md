# DCP Intermission Calculator

This program reads a [Digital Cinema Package (DCP) CompositionPlaylist XML file](https://en.wikipedia.org/wiki/Digital_Cinema_Package#Composition_playlist_file)
and prints the Reel end times in a human readable format. These times can be used for automated intermission placement.