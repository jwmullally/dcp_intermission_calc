#!/usr/bin/env python

"""
DCP Intermission Calculator

Authors:
    Joe Mullally (jwmullally@gmail.com)
    Vivi Sereti (vivithegoddess@gmail.com)

LICENSE: MIT
"""

import re


def seconds_to_time(seconds):
    hours_result, hrs_seconds = divmod(seconds, 3600)
    minutes_result, seconds_result = divmod(hrs_seconds, 60)
    time_str = '%02d:%02d:%02d' % (hours_result, minutes_result, seconds_result)
    return time_str
    
def frames_to_time(num_frames):
    seconds = round(num_frames / 24)
    time = seconds_to_time(seconds)
    return time

def reels_to_time (reels):
    result_times = []
    total_frames = 0
    for frames in reels:
        total_frames = total_frames + frames
        time = frames_to_time(total_frames)
        result_times.append (time)
    return result_times

def xml_get_reel_frames(xmlstr):
    """
    Return a list of MainPicture Reel durations in frames from a DCP xml
    Example:
        get_reel_frames(xmlstr)
        > [29809, 30000, 30000, 30000, 3911]
    """
    reel_frames = []
    for mainpic in re.findall('<MainPicture>(.*?)</MainPicture>', xmlstr):
        fps_str = re.findall('<FrameRate>(.*?)</FrameRate>', mainpic)[0]
        if fps_str != '24 1':
            raise ValueError('Unhandled FrameRate: ' + fps_str)
        duration = int(re.findall('<Duration>(.*?)</Duration>', mainpic)[0])
        reel_frames.append(duration)
    return reel_frames

print('1: Enter list manually')
print('2: Read from XML')
mode = input('enter mode: ')
if mode == '1':
    print('Enter Reel Durations, seperated by space')
    print('Example:')
    print('23950 29584 23986 21958')
    frames_input = input('> ')
    reel_frames = [int(x) for x in frames_input.split()]   
elif mode == '2':
    xml_file = input('Enter XML file path: ')
    xmlstr = open(xml_file).read()
    xmlstr = xmlstr.replace('\n', '')
    if '<CompositionPlaylist' not in xmlstr or '<ContentKind>feature</ContentKind>' not in xmlstr:
        raise ValueError('File does not look like a DCP Feature playlist XML')
    title = re.findall('<ContentTitleText>(.*?)</ContentTitleText>', xmlstr)[0]
    print('Title:', title)
    reel_frames = xml_get_reel_frames(xmlstr)
else:
    print('Unknown mode, please enter 1 or 2')
    exit(1)

#reel_frames = [23950, 29584, 23986, 21958]
print ('Input reels Durations (in frames):', reel_frames)
result = reels_to_time(reel_frames)
print ('Reel end times (HH:MM:SS): ', result)
